This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Origin task description:

Create an app with a single page that has one input. The input takes a city name. The user can run a search with that name that will send the info to the backend, run a search for the weather using https://openweathermap.org/ api. It will store the results in the database and then show a history of the results back to the user to view on the same page as the input.
