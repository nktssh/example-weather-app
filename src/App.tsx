import React, { useCallback, useEffect, useState } from "react";
import "./App.css";
import { WeatherData } from "../server/weather/weather.types";

const App: React.FC = () => {
  const [city, setCity] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [allData, setAllData] = useState([] as WeatherData[]);
  const [error, serError] = useState("");

  useEffect(() => {
    fetch("/api/weather/all")
      .then(res => res.json())
      .then(response => {
        setAllData(response);
        setIsLoading(false);
      });
  }, []);

  const onFormSubmit = useCallback(
    e => {
      e.preventDefault();
      fetch(`/api/weather/${encodeURIComponent(city)}`)
        .then(res => res.json())
        .then(response => {
          if (response.error) {
            console.error(response);
            serError(response.message);
          } else {
            setAllData([...allData, response]);
            setCity("");
            serError("");
          }

          setIsLoading(false);
        });
    },
    [allData, city]
  );

  return (
    <div className="App">
      <form onSubmit={onFormSubmit} className="App-search">
        <label htmlFor="city" className="App-search-item App-search-label">
          Enter city:
        </label>
        <input
          type="text"
          id="city"
          value={city}
          className="App-search-item App-search-input"
          onChange={e => setCity(e.target.value)}
        />
        <button type="submit" className="App-search-item App-search-submit">
          Get weather
        </button>
        {error && <div className="App-error">{error}</div>}
      </form>
      <div className="App-results">
        <h1>History of queries and weather</h1>
        <div className="App-results-table">
          {isLoading ? (
            "Loading data for you..."
          ) : !allData.length ? (
            <div>Nothing to show yet...</div>
          ) : (
            allData.map(({ id, name, weather, main }: WeatherData) => (
              <div key={id} className="App-results-table-item">
                {name}&nbsp;—&nbsp;
                {weather.map(({ id: weatherId, description }) => (
                  <span key={weatherId}>{description}</span>
                ))}
                , {main.temp} ˚C
              </div>
            ))
          )}
        </div>
      </div>
    </div>
  );
};

export default App;
