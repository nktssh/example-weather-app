// import path from "path";
import { apiRouter } from "./api";
import express, { Request, Response } from "express";

const port = process.env.PORT || 8080;

export const app = express();

app.get("/ping", function(req: Request, res: Response) {
    return res.send("pong");
});
app.use("/api", apiRouter);

// app.use(express.static(path.join(__dirname, "build")));
// app.use(express.static(__dirname));
//
// app.get("/*", function(req, res) {
//     res.sendFile(path.join(__dirname, "build", "index.html"));
// });

app.listen(port);
