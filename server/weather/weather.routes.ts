import { Request, Response, Router } from "express";
import axios from "axios";

import { WeatherData, WeatherError } from "./weather.types";
import { getAllWeatherItems, saveOrUpdateWeatherItem } from "./weather.db";

const OPEN_WEATHER_TOKEN = "9a4032295b2355e4f45a3991c6c5205a";

function isError(data: WeatherData | WeatherError): data is WeatherError {
  return (data as WeatherError).cod === "404";
}

export const weatherRouter = Router();

weatherRouter.get("/all", async (req: Request, res: Response) => {
  const weatherItems = getAllWeatherItems();
  return res.send(weatherItems);
});

weatherRouter.get("/:city", async (req: Request, res: Response) => {
  const { city } = req.params;
  console.log(`Getting weather data for city: ${city}`);
  const data = await axios
    .get("http://api.openweathermap.org/data/2.5/weather", {
      params: {
        q: city,
        APPID: OPEN_WEATHER_TOKEN,
        units: "metric" // to display info in Celsius
      }
    })
    .then(response => response.data)
    .catch(error => {
      console.error(error.response.data);
      return error.response.data;
    });

  if (!data) {
    return res.json({ error: true, message: "Something went wrong" });
  }

  if (isError(data)) {
    res.statusCode = Number(data.cod);
    return res.json({ error: true, message: data.message });
  }

  res.json(data);

  saveOrUpdateWeatherItem(data);
});
