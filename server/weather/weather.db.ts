import Locallydb from "locallydb";
import { WeatherData } from "./weather.types";

const db = new Locallydb("./weather-db");
const collection = db.collection("weather");

export function getAllWeatherItems() {
  return collection.items;
}

export function saveOrUpdateWeatherItem(data: WeatherData) {
  const { name } = data;
  const item = collection.where({ name });
  if (item && item.cid) {
    collection.update(item.cid, data);
  } else {
    collection.insert(data);
  }
  collection.save();
}
